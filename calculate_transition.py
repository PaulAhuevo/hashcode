class Image:
    def __init__(self, tags):
        self.tags = tags

class Transition:
    def __init__(self, img1, img2):
        self.img1 = img1
        self.img2 = img2
        self.common_tags = self.get_common_tags(self.img1, self.img2)
        self.unique_tags_1 = self.get_unique_tags(img1, img2)
        self.unique_tags_2 = self.get_unique_tags(img2, img1)
        self.interest_score = Math.min(len(self.common_tags), len(self.unique_tags_1), len(self.unique_tags_2))


    def get_common_tags(self, img1, img2):
        common_tags = []
        for tag in img1.tags:
            if tag in img2.tags:
                common_tags.append(tag)
        return common_tags


    def get_unique_tags(self, img1, img2):
        unique_tags = []
        for tag in img1.tags:
            if tag not in img2:
                unique_tags.append(tag)
        return unique_tags
